#ifndef _APP_SYSTEM_H_
#define _APP_SYSTEM_H_
#include "generic.h"
#include "system_stm32f4xx.h"
#include "misc.h"
#include "stm32f4xx_tim.h"
#include "stm32f4xx_rcc.h"


void system_initialize(void);



#endif // _APP_SYSTEM_H_
