#include "system.h"
#include <stdint.h>
#include "stdio.h"
#include "generic.h"
#include "system_stm32f4xx.h"
#include "misc.h"
#include "stm32f4xx_conf.h"

    volatile uint8_t FLAG_ECHO = 0;
    volatile uint16_t SonarValue;
    GPIO_InitTypeDef port;
    TIM_TimeBaseInitTypeDef timer;
    TIM_OCInitTypeDef timerPWM;

    GPIO_InitTypeDef GPIO_InitStruct;
    EXTI_InitTypeDef EXTI_InitStruct;
    NVIC_InitTypeDef NVIC_InitStruct;
    GPIO_InitTypeDef gpio_cfg;

    int rotate_vector = 0;
    int pwm_enable = 1;
    int PWM_up = 1;

    void (*func_ch1_en)();
    void (*func_ch1_dis)();
    void (*func_ch2_en)();
    void (*func_ch2_dis)();

    void Soft_Delay(volatile uint32_t number)
    {
            while(number--);
    }

    void hall_input_init() {

        GPIO_StructInit(&gpio_cfg);



        RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
        GPIO_InitStruct.GPIO_Pin = GPIO_Pin_12;// | GPIO_Pin_8;// | GPIO_Pin_10;
        GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
        GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
        GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;
        GPIO_Init(GPIOD, &GPIO_InitStruct);


        RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
        GPIO_InitStruct.GPIO_Pin = GPIO_Pin_0;// | GPIO_Pin_6 | GPIO_Pin_5;
        GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN;
        GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;
        GPIO_Init(GPIOA, &GPIO_InitStruct);




/*
        EXTI_InitStruct.EXTI_Line = EXTI_Line12 | EXTI_Line11;
        EXTI_InitStruct.EXTI_LineCmd = ENABLE;
        EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
        EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
        EXTI_Init(&EXTI_InitStruct);

        GPIO_EXTILineConfig(GPIO_PortSourceGPIOC, GPIO_PinSource11);
        GPIO_EXTILineConfig(GPIO_PortSourceGPIOC, GPIO_PinSource12);


        NVIC_InitStruct.NVIC_IRQChannel = EXTI15_10_IRQn;
        NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0x00;
        NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0x00;
        NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
        NVIC_Init(&NVIC_InitStruct);


        GPIO_StructInit(&port);
        port.GPIO_Mode = GPIO_Mode_AF_PP;
        port.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_2;
        port.GPIO_Speed = GPIO_Speed_50MHz;
        GPIO_Init(GPIOA, &port);
*/


        RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE, ENABLE);
        GPIO_InitStruct.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9;
        GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
        GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
        GPIO_Init(GPIOE, &GPIO_InitStruct);

        RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);
        GPIO_PinAFConfig(GPIOE, GPIO_PinSource8, GPIO_AF_TIM1);
        GPIO_PinAFConfig(GPIOE, GPIO_PinSource9, GPIO_AF_TIM1);
        // timer_tick_frequency = Timer_default_frequency / (prescaller_set + 1)   -> prescaller_set = (Timer_default_frequency/timer_tick_frequency) - 1
        // 40kHz * 100point = 40Mhz -> prescaler = 168/40 - 1 = 3.2 -> 168/42 - 1 = 4 - 1 = 3 -> prescaler 3 == tick 42MHz
        //TIM_Period = timer_tick_frequency / PWM_frequency - 1 = 42MHz/40khz - 1 = 1050 - 1

        TIM_TimeBaseStructInit(&timer);
        timer.TIM_Prescaler = SystemCoreClock / 42000000; //168 000 000 /168 000 00 - 1 = 10*100kHz
        timer.TIM_Period = 1050 - 1;
        timer.TIM_ClockDivision = TIM_CKD_DIV1;
        timer.TIM_CounterMode = TIM_CounterMode_Up;
        timer.TIM_RepetitionCounter = 0;
        TIM_TimeBaseInit(TIM1, &timer);

        // pulse_length = ((TIM_Period + 1) * DutyCycle) / 100 - 1
        // PWM mode 2 = Clear on compare match
        // PWM mode 1 = Set on compare match

        TIM_OCStructInit(&timerPWM);
        timerPWM.TIM_Pulse = 525 - 1;
        timerPWM.TIM_OCMode         = TIM_OCMode_PWM2;
        timerPWM.TIM_OutputState    = TIM_OutputState_Enable;
        timerPWM.TIM_OutputNState   = TIM_OutputState_Enable;
        timerPWM.TIM_OCPolarity     = TIM_OCPolarity_High;
        timerPWM.TIM_OCNPolarity    = TIM_OCPolarity_High;
        timerPWM.TIM_OCIdleState    = TIM_OCIdleState_Reset;
        timerPWM.TIM_OCNIdleState   = TIM_OCIdleState_Reset;
        TIM_OC1Init(TIM1, &timerPWM);

        TIM_Cmd(TIM1, ENABLE);

    }





    void start_stop()
    {
        if ( pwm_enable == 0 )
            {
            TIM_Cmd(TIM1, ENABLE);
            pwm_enable = 1;
            }
        else
            {
            TIM_Cmd(TIM1, DISABLE);
            pwm_enable = 0;
            }

        GPIO_WriteBit(GPIOD, GPIO_Pin_12, pwm_enable);
    }


    void PWM_modifire()
    {
        if ( PWM_up )
            TIM2->ARR += 5;
        else
            TIM2->ARR -= 5;

        if ( TIM2->ARR > 110)
            PWM_up = 0;

        if ( TIM2->ARR < 105)
            PWM_up = 1;


    }


    int main(void)
    {

        SystemInit();


        hall_input_init();


        int Button_down_0 = 0;

        while(1)
        {

        Soft_Delay(1000000);
        /*if ( GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0) == 1 )
        {
            if ( Button_down_0 == 0 )
            {
                GPIO_WriteBit(GPIOC, GPIO_Pin_9, 1);
                Button_down_0 = 1;
            }
        }
        else
        {
            Button_down_0 = 0;
            GPIO_WriteBit(GPIOC, GPIO_Pin_9, 0);
        }*/

        if ( GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0) == 0 )
        {
            if ( Button_down_0 == 0 )
            {
                start_stop();
                Button_down_0 = 1;
            }
        }
        else
            Button_down_0 = 0;
/*
        if ( GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_6) == 0 )
        {
            if ( Button_down_1 == 0 )
            {
                change_rotate_vector();
                Button_down_1 = 1;
            }
        }
        else
            Button_down_1 = 0;

        if ( GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_7) == 0 )
        {
            if ( Button_down_2 == 0 )
            {
                PWM_modifire();
                Button_down_2 = 1;
            }
        }
        else
            Button_down_2 = 0;
*/
        }

    }


    /*GPIO_WriteBit(GPIOC, GPIO_Pin_10, 1);

    GPIO_WriteBit(GPIOD, GPIO_Pin_2, 0);*/
    /*

    Soft_Delay(10000000);

    rotate_backward();*/
    /*GPIO_WriteBit(GPIOC, GPIO_Pin_10, 0);
    GPIO_WriteBit(GPIOD, GPIO_Pin_2, 1);*/

    /*    	if ( TIM2->ARR > 900 )
        TIM2->ARR = 100;
    else
        TIM2->ARR += 1;*/

    /*
            GPIO_InitStruct.GPIO_Pin = GPIO_Pin_2;
            GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
            GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
            GPIO_Init(GPIOD, &GPIO_InitStruct);

            GPIO_InitStruct.GPIO_Pin = GPIO_Pin_0;
            GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IPU;
            GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
            GPIO_Init(GPIOA, &GPIO_InitStruct);

            GPIO_EXTILineConfig(GPIO_PortSourceGPIOA, GPIO_PinSource0);

            EXTI_InitStruct.EXTI_Line = EXTI_Line0;
            EXTI_InitStruct.EXTI_LineCmd = ENABLE;
            EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
            EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
            EXTI_Init(&EXTI_InitStruct);

            NVIC_InitStruct.NVIC_IRQChannel = EXTI0_IRQn;
            NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0x00;
            NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0x00;
            NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
            NVIC_Init(&NVIC_InitStruct);

    */


